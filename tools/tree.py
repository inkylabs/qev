#!/usr/bin/env python3
import argparse
import crayons
import json
import os
import re
import sys

import nltk
from nltk.tree.parented import ParentedTree


def matches(nodes, m, debug=False):
    nlist = []
    for n in nodes:
        if type(n) == str:
            continue
        text = n.label()
        for c in n:
            if type(c) == str:
                text += ':' + c
        nlist.append(text)
    text = ' '.join(nlist)
    ret = m.search(text)
    if debug:
        print(text, m, bool(ret))
    return ret


def get_preceding(n):
    nodes = []
    s = n.left_sibling()
    while s:
        nodes.append(s)
        s = s.left_sibling()
    nodes.reverse()
    return nodes


def get_subsequent(n):
    nodes = []
    s = n.right_sibling()
    while s:
        nodes.append(s)
        s = s.right_sibling()
    return nodes


def get_subsequent_flat(n):
    nodes = []
    p = n
    while p:
        nodes.extend(get_subsequent(p))
        p = p.parent()
    return flatten(*nodes)


def flatten(*nodes):
    ret = []
    for n in nodes:
        ret.append(n)
        if isinstance(n, nltk.tree.Tree):
            ret.extend(flatten(*n))
    return ret


def test_node(n, args):
    if args.node_pattern:
        if not matches([n], args.node_pattern):
            return False
    if args.node_flat_pattern:
        if not matches(flatten(n), args.node_flat_pattern):
            return False
    if args.child_sibling_pattern:
        if not matches(n, args.child_sibling_pattern):
            return False
    if args.preceding_sibling_pattern:
        if not matches(get_preceding(n), args.preceding_sibling_pattern):
            return False
    if args.subsequent_sibling_pattern:
        if not matches(get_subsequent(n), args.subsequent_sibling_pattern):
            return False
    if args.subsequent_flat_pattern:
        if not matches(get_subsequent_flat(n), args.subsequent_flat_pattern):
            return False
    if args.not_node_flat_pattern:
        if matches(flatten(n), args.not_node_flat_pattern):
            return False
    if args.not_child_sibling_pattern:
        if matches(n, args.not_child_sibling_pattern):
            return False
    if args.not_preceding_sibling_pattern:
        if matches(get_preceding(n), args.not_preceding_sibling_pattern):
            return False
    if args.not_subsequent_sibling_pattern:
        if matches(get_subsequent(n), args.not_subsequent_sibling_pattern):
            return False
    if args.not_subsequent_flat_pattern:
        if matches(get_subsequent_flat(n),
                args.not_subsequent_flat_pattern):
            return False

    return True


def contains(t, m):
    for n in t:
        if type(n) == str:
            continue
        if m.match(n.label()):
            return True
        if isinstance(n, nltk.tree.Tree):
            ret = contains(n, m)
            if ret:
                return True
    return False


def explore(t, args):
    for n in t:
        if type(n) == str:
            continue
        if test_node(n, args):
            return n
        if isinstance(n, nltk.tree.Tree):
            ret = explore(n, args)
            if ret:
                return ret

    return None


def reconstruct(t):
    parts = []
    for n in t:
        if type(n) == str:
            parts.append(n)
        if isinstance(n, nltk.tree.Tree):
            parts.append(reconstruct(n))
    ret = ' '.join(parts)
    ret = re.sub(r' (?=[.:;,])', '', ret)
    return ret


def main():
    """The main routine."""
    # Parse arguments
    parser = argparse.ArgumentParser(description='parse trees')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                        help='print more stuff')
    parser.add_argument('--start', help='start ref')
    parser.add_argument('-n', '--node_pattern', help='node pattern')
    parser.add_argument('-nf', '--node_flat_pattern', help='node flat pattern')
    parser.add_argument('-c', '--child_sibling_pattern',
            help='child sibling pattern')
    parser.add_argument('-b', '--preceding_sibling_pattern',
            help='preceding sibling pattern')
    parser.add_argument('-a', '--subsequent_sibling_pattern',
            help='not subsequent sibling pattern')
    parser.add_argument('-af', '--subsequent_flat_pattern',
            help='subsequent flat pattern')
    parser.add_argument('-nnf', '--not_node_flat_pattern',
            help='not node flat pattern')
    parser.add_argument('-nc', '--not_child_sibling_pattern',
            help='not child sibling pattern')
    parser.add_argument('-nb', '--not_preceding_sibling_pattern',
            help='not preceding sibling pattern')
    parser.add_argument('-na', '--not_subsequent_sibling_pattern',
            help='not subsequent sibling pattern')
    parser.add_argument('-naf', '--not_subsequent_flat_pattern',
            help='not subsequent flat pattern')
    parser.add_argument('infile', help='file to read')
    args = parser.parse_args()

    for attr in dir(args):
        if attr.endswith('_pattern'):
            s = getattr(args, attr)
            if s:
                setattr(args, attr, re.compile(s))

    started = not args.start
    for line in open(args.infile):
        d = json.loads(line)
        ref, text, parse = d['ref'], d['text'], d['parse']
        started = started or start == ref
        if not started:
            continue

        t = ParentedTree.fromstring(parse)
        m = explore(t, args)
        #r = reconstruct(t)
        #if text != r:
        #    print(crayons.yellow('Was not able to construct text'))
        #    print('original:      ', ref, text)
        #    print('reconstruction:', ref, r)
        #    break
        if m:
            print(ref, text)
            print('match', crayons.red(m))
            print('parse', t)
    return 0


if __name__ == '__main__':
    try:
        exit(main())
    except KeyboardInterrupt:
        print('Exiting due to KeyboardInterrupt!', file=sys.stderr)
