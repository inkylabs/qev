#!/usr/bin/env python3
import argparse
import json
import os
import re
import sys

from nltk.parse.corenlp import CoreNLPServer, CoreNLPParser


STANFORD = '/home/cco3/Downloads/stanford-corenlp-4.5.0'
server = None


# sudo netstat -ltnp | grep -w ':9000'
# cd /home/cco3/Downloads/stanford-corenlp-4.5.0
#java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 9000 -timeout 15000


def main():
    """The main routine."""
    # Parse arguments
    parser = argparse.ArgumentParser(description='make trees')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                        help='print more stuff')
    parser.add_argument('--start', help='start ref')
    args = parser.parse_args()

    # Create the server
    print('Starting server…', end='', file=sys.stderr)
    server = CoreNLPServer(
       os.path.join(STANFORD, "stanford-corenlp-4.5.0.jar"),
       os.path.join(STANFORD, "stanford-corenlp-4.5.0-models.jar"),
    )
    server.start()
    print('done', file=sys.stderr)
    parser = CoreNLPParser()
    
    data = open('qev.txt').read()
    started = not args.start
    for m in re.finditer(r'{\*?([^\n]*)} ([^{]*)', data, re.M | re.S):
        ref, text = m[1], m[2].strip()
        text = re.sub(r'([][]|<.*?>)', '', text)
        text = re.sub(r'Jehovah God', 'Jehovah-God', text)
        started = started or args.start == ref
        if not started:
            continue

        parse = next(parser.raw_parse(text))
        print(json.dumps({
            'ref': ref,
            'text': text,
            'parse': str(parse),
        }))
    server.stop()


if __name__ == '__main__':
    try:
        exit(main())
    except KeyboardInterrupt:
        print('Exiting due to KeyboardInterrupt!', file=sys.stderr)
    finally:
        if server is not None:
            print('Stopping server', file=sys.stderr)
            server.stop()
