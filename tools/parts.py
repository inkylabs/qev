#!/usr/bin/env python3
import crayons
import nltk
import re
import sys

res = []
for a in sys.argv[1:]:
    res.append(re.compile(a))

def fits(full_str):
    if not res:
        return True
    m = res[0].search(full_str)
    if not m:
        return False
    return m.group()


data = open('qev.txt').read()
for m in re.finditer(r'{\*?([^\n]*)} ([^{]*)', data, re.M | re.S):
    ref, verse = m[1], m[2].strip()
    text = nltk.word_tokenize(verse)
    parts = nltk.pos_tag(text)
    parts_str = ' '.join(p[1] for p in parts)
    full_str = '{} {}'.format(verse, parts_str)
    m_str = fits(full_str)
    if m_str:
        print(ref, full_str.replace(m_str, str(crayons.red(m_str))))
