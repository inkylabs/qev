#!/bin/bash

GITLAB_USERNAME=$USER
BRANCH=q$RANDOM


git fetch
git checkout -b $BRANCH origin/master
vim qev.txt

if [[ `git status --porcelain --untracked-files=no` ]]; then
  git checkout master
  git branch -d $BRANCH
  exit
fi

git commit -a
git push -f $(git config --get remote.origin.url | sed "s/\(^https:\/\/gitlab.com\/\)[^/]*\//\1$GITLAB_USERNAME\//") -o merge_request.create -o merge_request.remove_source_branch -o merge_request.merge_when_pipeline_succeeds
git checkout master
