#!/bin/bash

GITLAB_USERNAME=$USER

git fetch
git rebase origin/master
vim qev.txt
git add qev.txt
git rebase --continue
git push -f $(git config --get remote.origin.url | sed "s/\(^https:\/\/gitlab.com\/\)[^/]*\//\1$GITLAB_USERNAME\//") -o merge_request.create -o merge_request.remove_source_branch -o merge_request.merge_when_pipeline_succeeds
